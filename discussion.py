# [Section] Python Class Review
# Recall how class are created in Python
# Declaration of a class is done using the class statement followed by the className

class SampleClass():
	# The class constructor or initialization method is called by Python every time an instance of class is created

	def __init__(self, year, graduation):
		self.year = year
		self.graduation = graduation

	# methods are functions inside a class
	def show_year(self):
		print(f"The current year is: {self.year}")
		print(f"The graduation is: {self.graduation}")


myObj = SampleClass(2023, 2024) # 2023 will be passed as the value of the year property
print(myObj.year)
myObj.show_year()


# [Section] Fundamentals of OOP
# There are 4 main fundamentals principles in OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstraction


# [Section] Encapsulation
# Encapsulation is a mechanism of wrapping the attributes and code acting on the methods together as a single unit
# In encapsulation, the attributes of a class will be hidden from other classes, and can be accesed only through the methods of their current class. Therefore it is also known as DATA HIDING.

# Parent/ Base class
class Person():

	def __init__(self):
		# protected attribute  (accessible in it's own class)
		self._name = "John Doe"

	# setter method set_name(change the value)
	def set_name(self, name):
		self._name = name

	# gettere method get_name
	def get_name(self):
		print(f"Name of Person: {self._name}")

	# Mini Activity
	# setter method
	def set_age(self, age):
		self._age = age

	# getter method
	def get_age(self):
		print(f"Age of Person: {self._age}")

p1 = Person()
p1.get_name()
print(p1._name)
p1.set_name("Bob Doe")
p1.get_name()
p1.set_age(16)
p1.get_age()


# [Section] Inheritance (can ACCESS in other class)
# The transfer of characteristics of a parent classes(base class) to child classes(derive class) that are derived from it
class Employee(Person):
	def __init__(self, employeeId):
		super().__init__()
		self._employeeId = employeeId

	# setter
	def set_employeeId(self, employeeId):
		self._employeeId = employeeId


	# getter
	def get_employeeId(self):
		print(f"The employee ID is {self._employeeId}")

	def get_details(self):
		print(f"{self._employeeId} belongs to {self._name}")

# Test Cases:
print("Employee test cases ------")
emp1 = Employee("Emp-IT1") # upon creating/initialization
emp1.get_details()
emp1.set_name("Johnny Doe")
emp1.set_employeeId("Emp-HR5")
emp1.get_details()



# Child/ Derived class
class Student(Person):

	def __init__(self, studentNo, course, year_level):

		# super() can be used to invoke immediate parent class constructor(IMPORTANT)
		super().__init__()
		# attributes unique to Employee class
		self._studentNo = studentNo
		self._course = course
		self._year_level = year_level

	# getters
	def get_studentNo(self):
		print(f"Student number of student is {self._studentNo}")

	def get_course(self):
		print(f"Course of student is {self._course}")

	def get_year_level(self):
		print(f"Year level of student is {self._year_level}")

	# setters
	def set_studentNo(self, studentNo):
		self._studentNo = studentNo

	def set_course(self, course):
		self._course = _course

	def set_year_level(self, year_level):
		self._year_level = year_level

	# get details
	def get_details(self):
		print(f"{self._name} is currently in year {self._year_level} taking up {self._course}")

# test cases = invocations

# Test Cases:
student1 = Student("stdt-001", "Computer Science", 1)
student1.set_name("Derek Doe")
student1.set_age(24)
student1.get_details() # invocation



# ---------------------------------------------------------------------------------------------------------

# [Section] Polymorphism
# A child class inherits all the methods from the parent class. However, in some situations, the method inherited from the parent class doesnt quite fit into the child class. In such cases, you will re-implement mthod in the child class.

# There are different methods to use polymorphism in Python. You can use different function, class methods or objects to define polymorphism.

print("------------------------------------------------------")

# Functions and Objects
# A function can be created that can take any object allowing polymorphism
class Admin():

	def is_admin(self):
		print(True)

	def user_type(self):
		print("Admin User")

class Customer():

	def is_admin(self):
		print(False)

	def user_type(self):
		print("Regular User")

# create object instances for Admin and customer
user_admin = Admin()
user_customer = Customer()


# define a test function that will take an object called obj
def test_function(obj):
	obj.is_admin()
	obj.user_type()



# pass the created instances to the test_function
test_function(user_admin)
test_function(user_customer)
	
# long method ----
# user_admin.is_admin()
# user_admin.user_type()
# user_customer.is_admin()
# user_customer.user_type()


# Polymorphism with class methods

# Python uses two different class in the same way.

print("--------------------------------------------------------------------")
class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth(self):
		print(True)

team_leader = TeamLead()
team_member = TeamMember()


# Let's use loop to iterate a collection of objects

for person in (team_leader, team_member):
	person.occupation() # displaying all outputs from occupation functions of all objects(team_leader, team_member)
	person.hasAuth()



# Polymorphism with Inheritance ------------------------------------------------------------------------------------------------

# Parent class
class Zuitt():

	def tracks(self):
		print("We are currently offering a trcks, (developer career, pi-shape career, and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours!")

# Child class
		# Child		# Parent
class DeveloperCareer(Zuitt):

	# OVERRIDE the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn sthe basics of web development in 240 hours!")


class PiShapeCareer(Zuitt):

	# OVERRIDE the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn skills for no-code app development in 140 hours!")


class ShortCourses(Zuitt):

	# OVERRIDE the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn advanced topics in web development in 20 hours!")

course1 = DeveloperCareer()
course2 = PiShapeCareer()
course3 = ShortCourses()


# LONG method
print("----------------------")
course1.tracks()
course1.num_of_hours()

print("----------------------")
course2.tracks()
course2.num_of_hours()

print("----------------------")
course3.tracks()
course3.num_of_hours()


# [Section] Abstraction --------------------------------------------------------------------------------

# An abstract class can be considered as a blueprint for other classes. It allows you to create a set of methods that must be created within any child built from abstract class
# A class which contains 1 or more abstract methods is called an abstract class

# By default, Python does not provide abstract classes. Python omes with a module that provides the base for defining Abstract Base Classes (ABC) and that module name is ABC


from abc import ABC, abstractclassmethod #REQUIRED for Abstract
# The import tells the program to get the abc module of Python to be used
# ABC means ABSTRACT BASE CLASSES - This module provides the infrastracture for defining abstract bse classes


# The class Polygon inherits the abstract class module
class Polygon(ABC):

	# Create an abstract method called printNumberOfSides that needs to be implemented by classes inherit Polygon
	@abstractclassmethod
	def printNumberOfSides(self): #This method doesn't do anything but was made to serve as blue print for other classes

		# This method will be created by the child class
		# The pass keyword denotes that the method doesn't do anything
		pass

	@abstractclassmethod
	def numberOfCorners(self):
		pass

	# @abstractclassmethod
	# def angleDegree(self):
	# 	pass

# Number of functions in parent must be done with the children!!!-----

# Child/Derived Class
class Hexagon(Polygon):

	def __init__(self):
		super().__init__() # super() can be used to invoke immediate parent class constructor(IMPORTANT)


	def printNumberOfSides(self):
		print("This polygon has 6 sides.")

	def numberOfCorners(self):
		print("This polygon has 6 vertices (or corners).")

class Square(Polygon):

	def __init__(self):
		super().__init__() # super() can be used to invoke immediate parent class constructor(IMPORTANT)


	def printNumberOfSides(self):
		print("This polygon has 4 sides.")

	def numberOfCorners(self):
		print("This polygon has 4 vertices (or corners).")

shape1 = Hexagon()
shape1.printNumberOfSides()

shape2 = Square()
shape2.printNumberOfSides()



# Additional Example
# Abstraction with Inheritance

# from abc import ABC, abstractclassmethod

# Parent/Base Class
class Vehicle(ABC):

	@abstractclassmethod
	def numberOfWheel(self, wheel):
		pass

	@abstractclassmethod
	def capacity(self, capacity):
		pass

	@abstractclassmethod
	def get_vehicle_sound(self):
		pass


# Derived class
class Kuliglig(Vehicle):

	def __init__(self, wheel, capacity):
		super().__init__() # super() can be used to invoke immediate parent class constructor
		self._numberOfWheel = wheel
		self._capacity = capacity

	def set_numberOfWheel(self, wheel) :
		self._numberOfWheel = wheel

	def set_capacity(self, capacity):
		self._capacity = capacity

	def numberOfWheel(self):
		print(f"This vehicle has {self._numberOfWheel} wheels")


	def capacity(self):
		print(f"This vehicle could carry {self._capacity} persons")

	def get_vehicle_sound(self):
		print("kuh liglig liglig lig")


kuliglig = Kuliglig(2,4)
kuliglig.numberOfWheel()
kuliglig.capacity()
kuliglig.get_vehicle_sound()